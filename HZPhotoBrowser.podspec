Pod::Spec.new do |s|
  s.name         = "HZPhotoBrowser"
  s.version      = "2.0.0"
  s.summary      = "iOS查看大图"
  s.homepage     = "https://gitee.com/Andney/HZPhotoBrowser"
  s.license      = "MIT"
  s.author       = { "Andney" => "1832004522@qq.com" }
  s.source       = { :git => "https://gitee.com/Andney/HZPhotoBrowser.git", :tag => s.version.to_s }
  s.source_files = 'HZPhotoBrowser/HZPhotoBrowser/*.{h,m}'
  s.requires_arc = true
  s.ios.deployment_target = '8.0'
  s.frameworks = 'CoreServices'
  s.resource  = "HZPhotoBrowser/HZPhotoBrowser/HZPhotoBrowser.bundle"
  s.dependency 'SDWebImage', '>= 5.0.0'
end
